#!/bin/sh
#SBATCH -J sra
#SBATCH -t 60
export BIOLITE_RESOURCES=database=$PWD/biolite.sqlite
bl-sra export --center Dunnlab --study PRJNA263637 $(<ids.txt)
