#!/bin/bash

#SBATCH -t 5:00:00
#SBATCH -c 1

module load raxml

python parsimony_trees.py
