#!/bin/bash

#SBATCH -t 48:00:00
#SBATCH -c 16
#SBATCH -C e5-2600

module load raxml

raxmlHPC-HYBRID -m PROTGAMMAWAG -s supermatrix1.phy -N 200 -n BS -f j -b $RANDOM -T 16 -q partition1.txt

#For partitioned analysis, replace partition1.txt for partition2.txt

#For analysis without unstable taxa, replace supermatrix1.phy for supermatrix3.phy and partition1.txt for partition3.txt
