#!/bin/bash
#SBATCH -J BayesianAnalysis_chain1
#SBATCH --nodes=8-8
#SBATCH --ntasks-per-node=4
#SBATCH -t 6-00:00:00
#SBATCH -n 32
#SBATCH --mem=60G

module load phylobayes/1.3b-mpi

#Run twice for 2 chains; change name of chain

srun pb_mpi -dc -d supermatrix1.phy -cat -poisson chain1

