#!/bin/bash
#SBATCH -t 14-00:00:00
#SBATCH -c 24

module load R/3.0.0;

#Change constree1.tre (Ocotocorallia sister to Medusozoa) for constree2.tre (Staurozoa sister to all Medusozoa) or constree3.tre (Cerianthid sister to all Anthozoa). Change also --name and output accordingly.
perl sowhat --constraint=constree1.tre --aln=supermatrix1.phy --model=PROTGAMMAWAG  --name=contree1_output --rax="raxmlHPC-PTHREADS -T 24"  > constree1_output --reps=500
