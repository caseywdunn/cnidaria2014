#Maximum likelihood bootstrap analyses: please read comments in each script to run 
#partitioned analysis and analysis removing unstable taxa
01-generate_bootstrap_reps.sh
02-generate_parsimony_trees.sh
03-run_examl.sh

#Bayesian phylogenetic analysis
BayesianInference.sh

#SOWH test
sowhat.sh
