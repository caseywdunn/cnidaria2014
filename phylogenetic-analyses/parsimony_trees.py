#!/usr/bin/env python

import os, sys
import random
import subprocess

basename="supermatrix1.phy.BS"
#For analysis without unstable taxa, above replace supermatrix1.phy.BS for supermatrix3.phy.BS

#For partitioned analysis, replace below partition1.txt for partition2.txt
#For analysis without unstable taxa, replace below partition1.txt for partition3.txt

for i in range(0,200):
        random_pars = random.randint(1, sys.maxint)
        cmd = 'raxmlHPC  -y -s {0}{1} -m PROTGAMMAWAG -q partition1.txt -n T{2} -p {3} -T 1'.format(
        basename, i, i, random_pars)
        print cmd
        subprocess.call(cmd, stdout=open(os.devnull, 'wb'), shell=True)

print "Done!"
