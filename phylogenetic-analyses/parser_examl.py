import os, sys
import random
import subprocess

basename="supermatrix1.phy.BS"
#For analysis without unstable taxa, replace above supermatrix1.phy.BS for supermatrix3.phy.BS
pars_treenamebase="RAxML_parsimonyTree.T"

#For partitioned analysis, replace below partition1.txt for partition2.txt
#For analysis without unstable taxa, replace below partition1.txt for partition3.txt

for i in range(0,200):
        #create a "partition.txt" file with a single partition; the file 
        #has a single line that looks like: 
        #WAG, p1=1-N #N is the length of the supermatrix
        cmd = "parser -s {0}{1} -m PROT -q partition1.txt -n {0}{1}".format(
                basename, i)
        subprocess.check_call(cmd, shell=True)  
        outfile = os.path.join(os.getcwd(), 'examl_submission' + str(i) + ".sh")
        with open(outfile, "w") as f:
                print >>f, "#!/bin/bash"
                print >>f, "#SBATCH -J ExaML_Bootstraps"+str(i)
                print >>f, "#SBATCH --nodes=8-8"
                print >>f, "#SBATCH --ntasks-per-node=8"
                print >>f, "#SBATCH -t 2:00:00"
                print >>f, "#SBATCH -n 64"
                print >>f, "#SBATCH -C e5-2600"+"\n"
                print >>f, "srun examl -s {0}{1}.binary -n result{1} -B 1 -m GAMMA -S -t {2}{1}".format(
                                basename, i, pars_treenamebase)
        subprocess.check_call(["sbatch", outfile])

print "Done!"
