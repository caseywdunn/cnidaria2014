## Phylogenetic data files for analyses presented in:

> Zapata F, Goetz FE, Smith SA, Howison M, Siebert S, Church SH, Sanders SM, Ames CL, McFadden CS
> France SC, Daly M, Collins AG, Haddock SHD, Dunn CW, Cartwright P. Phylogenomic analyses support 
> traditional relationships within Cnidaria. bioRxiv 

There are 13 files corresponding to:

* Supermatrices (2 files) 
* Partition files (3 files)
* Tree sets for maximum likelihood bootstrap replicates for each analysis (3 files)
* Maximum likelihood tree with bootstrap support values for each supermatrix (3 files)
* Tree sets for Bayesian inference (post-burn in) for each supermatrix (1 file)
* Majority rule consensus tree summarizing posterior probability (1 file)

Files are named following this convention:

* Supermatrices: supermatrix[1 used in all analyses/3 after removing the unstable taxa *Cerianthid* and *Haliclystus sanjuanensis*].phy
* Partition files: partition[1 concatenated analysis/2 partitions resulting from PartitionFinder/3 after removing the unstable taxa].txt
* Tree sets for Maximum likelihood bootstrap replicates: supermatrix[1/3]_(partitioned)_ML_BS_replicates.trees
* Maximum likelihood tree with bootstrap support values: supermatrix[1/3]_(partitioned)_ML_Tree_BS_support.tre
* Tree sets for Bayesian inference: supermatrix1_BI_posterior_chains1_2.trees
* Majority rule consensus tree summarizing posterior probability: supermatrix1_BI_MRCT.tre

These files correspond to the figures presented in the paper:

* `supermatrix1_ML_Tree_BS_support.tre` is **Figure 4**
* `supermatrix1_BI_MRCT.tre` is **Supplementary Figure 1**
* `supermatrix1_partitioned_ML_Tree_BS_support.tre` is **Supplementary Figure 2**
* `supermatrix3_ML_Tree_BS_support.tre` is **Supplementary Figure 3**

