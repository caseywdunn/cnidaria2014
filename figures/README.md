# Figures

This folder includes the following figures from the paper:

- Vector pdf files for the manuscript figures.
- png and svg of the summary figures for embedding in websites.
- Vector line illustrations of organisms made by Freya Goetz for the manuscript. These are also available at the [wikimedia commons](https://commons.wikimedia.org/wiki/Special:Search/FreyaGoetz). 


