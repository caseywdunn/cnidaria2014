#!/bin/sh

unset PYTHONPATH
module load agalma/0.4.0

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-cnidaria2014.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-cnidaria2014.sqlite,outdir=/gpfs/data/cdunn/analyses/cnidaria2014"

mkdir -p ~/data
cd ~/data

EMAIL="felipe_zapata@brown.edu"

sra import -c -e $EMAIL SRX288276 & #Abylopsis
sra import -c -e $EMAIL SAMN3418514 & #Aegina
sra import -c -e $EMAIL SRX288285 & #Agalma
sra import -c -e $EMAIL SRX231866 & #Aiptasia
sra import -c -e $EMAIL SAMN3418515 & #Atolla
sra import -c -e $EMAIL SRX288432 & #Craseoa
sra import -c -e $EMAIL SRX288430 & #Nanomia
sra import -c -e $EMAIL SRX288431 & #Physalia
sra import -c -e $EMAIL SAMN3418513 & #Alatina
wait

wget http://marinegenomics.oist.jp/genomes/download/adi_v1.0.1.cdna.fa.gz
gunzip adi_v1.0.1.cdna.fa.gz
agalma catalog insert --paths "adi_v1.0.1.cdna.fa" -s 'Acropora digitifera' -n '70779' -d '52864' -e '' -l '' -c '' --id 'OIST_ACRDIG' -t '' -q '' --note 'Gene predictions downloaded from OIST genome project' -b 'genome' --sample_prep ' | '

wget https://dl.dropboxusercontent.com/u/37523721/moya_transcr_amillepora_igmNN_withannotations.zip
unzip moya_transcr_amillepora_igmNN_withannotations.zip
agalma catalog insert --id "ext_ACRMIL" --paths amillepora_db_annotations_april2013/igmNN.fasta --species "Acropora millepora" --ncbi_id "45264" --itis_id "52901" --library_type "transcriptome" --note "Downloaded from the matzLAB (http://www.bio.utexas.edu/research/matz_lab/matzlab/Data.html), see also DOI: 10.1111/j.1365-294X.2012.05458.x" --sample_prep " | "

wget ftp://ftp.jgi-psf.org/pub/JGI_data/Amphimedon_queenslandica/annotation/Aqu1.cds.fa.gz
gunzip Aqu1.cds.fa.gz
agalma catalog insert --id "JGI_AMPQUE" --paths Aqu1.cds.fa --species "Amphimedon queenslandica" --ncbi_id "400682" --itis_id "659484" --library_type "genome" --note "Itis ID for genus.  Downloaded predictions from JGI genome annotation " --sample_prep " | "

agalma catalog insert --id "dbEST_ANEVIR" --paths /gpfs/data/cdunn/sequences/public/dbEST/Anemonia_viridis/postcluster/processed.nc.fasta --species "Anemonia viridis" --ncbi_id "51769" --itis_id "204180" --library_type "transcriptome" --note "Assembled EST by SSmith from Cnidaria Tree of Life" --sample_prep " | "

agalma catalog insert --id "454_ANTHOM" --paths /gpfs/data/cdunn/analyses/assembly/Anthomastus_newbler_ssmith_07May11/P_2011_04_07_14_53_45_runAssembly/assembly/454Exemplar.fna --species "Anthomastus" --ncbi_id "86531" --itis_id "52030" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Catalog the exemplars he already selected" --sample_prep " | "

agalma catalog insert --id "454_ANTGRI" --paths /gpfs/data/cdunn/analyses/assembly/Antipathes_griggi_newbler_ssmith_07May11/P_2011_04_13_11_06_51_runAssembly/assembly/454Exemplar.fna --species "Antipathes griggi" --ncbi_id "639590" --itis_id "51942" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected.  ITIS id for genus" --sample_prep " | "

agalma catalog insert --paths /gpfs/data/cdunn/sequences/public/SRA/Aurelia_aurita/Aurelia_aurita-Roscoff-Assembly/Aurelia_aurita.Roscoff.exemplars.fasta -s 'Aurelia aurita' -n '6145' -d '51701' -e '' -l '' -c '' --id '454_AURAUR' -t '' -q '454' --note 'Dowloaded 5 libraries: SRR040475-SRR040479 and assembled with Newbler on January 2014' -b 'transcriptome' --sample_prep ' | '

agalma catalog insert --id "454_CALFRA" --paths /gpfs/data/cdunn/analyses/assembly/Calibemnon_newbler_ssmith_08May11/P_2011_04_08_08_49_18_runAssembly/assembly/454Exemplar.fna --species "Calibelemnon francei" --ncbi_id "86567" --itis_id "719046" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected.  NCBI id for family, ITIS id for genus" --sample_prep " | "

agalma catalog insert --id "454_CANDEL" --paths /gpfs/data/cdunn/analyses/assembly/Candelabrium_newbler_ssmith_07May11/P_2011_04_07_15_36_28_runAssembly/assembly/454Exemplar.fna --species "Candelabrum sp." --ncbi_id "264049" --itis_id "49133" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected.  NCBI id for genus, ITIS id for genus" --sample_prep " | "

wget ftp://ftp.jgi-psf.org/pub/JGI_data/Capitella/v1.0/FilteredModelsv1.0.na.fasta.gz
gunzip FilteredModelsv1.0.na.fasta.gz
agalma catalog insert --id "JGI_CAPTE1" --paths FilteredModelsv1.0.na.fasta --species "Capitella teleta" --ncbi_id "283909" --itis_id "67414" --library_type "genome"

agalma catalog insert --id "454_CERIAN" --paths /gpfs/data/cdunn/analyses/assembly/Cerianthid_newbler_ssmith_07May11/P_2011_04_07_15_42_48_runAssembly/assembly/454Exemplar.fna --species "Cerianthid" --ncbi_id "37513" --itis_id "51985" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected.  NCBI id for family, ITIS id for family" --sample_prep " | "

agalma catalog insert --id "dbEST_CLYHEM" --paths /gpfs/data/cdunn/sequences/public/dbEST/Clytia_hemisphaerica/postcluster/processed.nc.fasta --species "Clytia hemisphaerica" --ncbi_id "252671" --itis_id "49583" --library_type "transcriptome" --note "Assembled EST from Cnidaria Tree of Life" --sample_prep " | "

agalma catalog insert --id "454_CRASPE" --paths /gpfs/data/cdunn/analyses/assembly/Craspedacusta_newbler_ssmith_07May11/P_2011_04_08_09_41_29_runAssembly/assembly/454Exemplar.fna --species "Craspedacusta sowerbii" --ncbi_id "128124" --itis_id "50776" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected" --sample_prep " | "

agalma catalog insert --id "FLYBASE_DROMEL" --paths /gpfs/data/cdunn/sequences/public/flybase/Drosophila_melanogaster/dmel-all-transcript-r5.31.fasta --species "Drosophila melanogaster" --ncbi_id "7227" --itis_id "146290" --library_type "transcriptome" --sample_prep " | "

agalma catalog insert --id "HWI-ST420-69-D0F2NACXX-3-ECTOPLEURA" --paths /gpfs/data/cdunn/sequences/collaborators/cnidaria_paulyn/Ectopleura_larynx/Ectopleura_R1.chast.fastq.gz /gpfs/data/cdunn/sequences/collaborators/cnidaria_paulyn/Ectopleura_larynx/Ectopleura_R2.chast.fastq.gz -s 'Ectopleura larynx' -n '264052' -d '719103' -e '' -l '' -c '' -t '' -q 'Illumina HiSeq' --note 'Data generated in the lab of Paulyn Cartwright (Kansas)' -b 'transcriptome' --sample_prep ' | '

agalma catalog insert --id "454_HALICL" --paths /gpfs/data/cdunn/analyses/assembly/Haliclystus_newbler_ssmith_04Apr11/P_2011_04_08_09_31_27_runAssembly/assembly/454Exemplar.fna --species "Haliclystus sanjuanensis" --ncbi_id "168739" --itis_id "51486" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected" --sample_prep " | "

agalma catalog insert --id "454_HALVAL" --paths /gpfs/data/cdunn/analyses/assembly/Halitrephes_valdiviae_newbler_ssmith_07May11/P_2011_05_25_16_29_12_runAssembly/assembly/454Exemplar.fna --species "Halitrephes valdiviae" --ncbi_id "168740" --itis_id "719177" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected.  NCBI id for family" --sample_prep " | "

wget http://ftp.ncbi.nih.gov/genomes/Hydra_magnipapillata/RNA/rna.fa.gz
gunzip rna.fa.gz
agalma catalog insert --id "NCBI_HYDMAG" --paths rna.fa --species "Hydra magnipapillata" --ncbi_id "6085" --itis_id "50845" --library_type "genome" --note "Gene predictions from genome sequencing" --sample_prep " | "

agalma catalog insert --paths /gpfs/data/cdunn/sequences/collaborators/cnidaria_paulyn/Hydractinia_symbiolongicarpus/Hydractinia_R1.fq.gz /gpfs/data/cdunn/sequences/collaborators/cnidaria_paulyn/Hydractinia_symbiolongicarpus/Hydractinia_R2.fq.gz -s 'Hydractinia symbiolongicarpus' -n '13093' -d '719085' -e '' -l '' -c '' --id 'HISEQ-129-C25BFACXX-8-HYDRACTINIA' -t '' -q 'Illumina HiSeq' --note 'Data generated in the lab of Paulyn Cartwright (Kansas)' -b 'transcriptome' --sample_prep ' | '

agalma catalog insert --id "454_ISIDEL" --paths /gpfs/data/cdunn/analyses/assembly/Isidella_newbler_ssmith_07May11/P_2011_04_07_15_50_12_runAssembly/assembly/454Exemplar.fna --species "Keratoisidinae sp." --ncbi_id "444732" --itis_id "52329" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected. This sample was originally IDd as Isidella, but Scott France confirms it is a yet unidentified Keratoisidinae sp. So ID says Isidella, but the name and NCBI/ITIS ID are fixed" --sample_prep " | "

agalma catalog insert --id "dbEST_METSEN" --paths /gpfs/data/cdunn/sequences/public/dbEST/Metridium_senile/postcluster/processed.nc.fasta --species "Metridium senile" --ncbi_id "6116" --itis_id "52737" --library_type "transcriptome" --note "Assembled EST by SSmith from Cnidaria Tree of Life" --sample_prep " | "

wget http://research.nhgri.nih.gov/mnemiopsis/download/transcriptome/ML2.2.nt.gz
gunzip ML2.2.nt.gz
agalma catalog insert --id "NHGRI_MNELEI" --paths ML2.2.nt --species "Mnemiopsis leidyi" --ncbi_id "27923" --itis_id "53917" --library_type "transcriptome" --note "Downloaded transcriptome from NHGRI" --sample_prep " | "

agalma catalog insert --id "dbEST_MONFAV" --paths /gpfs/data/cdunn/sequences/public/dbEST/Montastraea_faveolata/postcluster/processed.nc.fasta --species "Montastraea faveolata" --ncbi_id "48498" --itis_id "572291" --library_type "transcriptome" --note "Assembled EST by SSmith from Cnidaria Tree of Life" --sample_prep " | "

wget ftp://ftp.jgi-psf.org/pub/JGI_data/Nematostella_vectensis/v1.0/annotation/transcripts.Nemve1FilteredModels1.fasta.gz
gunzip transcripts.Nemve1FilteredModels1.fasta.gz
agalma catalog insert --id "JGI_NEMVEC" --paths transcripts.Nemve1FilteredModels1.fasta --species "Nematostella vectensis" --ncbi_id "45351" --itis_id "52498" --library_type "genome" --note "Predictions from JGI" --sample_prep " | "

agalma catalog insert --id "454_NEPHTH" --paths /gpfs/data/cdunn/analyses/assembly/Nephthyigorgia_newbler_ssmith_07May11/P_2011_04_07_15_56_37_runAssembly/assembly/454Exemplar.fna --species "Nephthyigorgia" --ncbi_id "361018" --itis_id "52054" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected.  ITIS id for family" --sample_prep " | "

agalma catalog insert --id "454_OBELON" --paths /gpfs/data/cdunn/analyses/assembly/Campahularid_newbler_ssmith_08May11/P_2011_04_08_09_21_12_runAssembly/assembly/454Exemplar.fna --species " Obelia longissima" --ncbi_id "32570" --itis_id "49515" --library_type "transcriptome" --sequencer "454" --note "Assembled EST by SSmith from Cnidaria Tree of Life" --sample_prep " | "

agalma catalog insert --id "HISEQ-134-C27MTACXX-2-PODOCORYNA" --paths /gpfs/data/cdunn/sequences/collaborators/cnidaria_paulyn/Podocoryna/Podocoryna_R1.fq.gz /gpfs/data/cdunn/sequences/collaborators/cnidaria_paulyn/Podocoryna/Podocoryna_R2.fq.gz -s 'Podocoryna carnea' -n '6096' -d '719298' -e '' -l '' -c '' -t '' -q 'Illumina HiSeq' --note "Combined Med1 and RP135 libraries from Paulyn's lab.  Med1 is medusa and RP135 is polyp with medusae buds at some developmental stage." -b 'transcriptome' --sample_prep ' | '

wget http://www.bio.utexas.edu/research/matz_lab/matzlab/Data_files/P_astreoides_db_annot_april2013.zip
unzip P_astreoides_db_annot_april2013.zip
head cat P_astreoides_db_annot_april2013/past_nozoox_apr13.fas | sed '/^>/ s/|/_/g' > P_astreoides_db_annot_april2013/past_nozoox_apr13_fixedHeaders.fas
rm P_astreoides_db_annot_april2013/past_nozoox_apr13.fas
agalma catalog insert --id "ext_PORAST" --paths P_astreoides_db_annot_april2013/past_nozoox_apr13_fixedHeaders.fas --species "Porites astreoides" --ncbi_id "104758" --itis_id "53184" --library_type "transcriptome" --note "Downloaded from the matzLAB (http://www.bio.utexas.edu/research/matz_lab/matzlab/Data.html)" --sample_prep " | "

agalma catalog insert --id "454_SCLERO" --paths /gpfs/data/cdunn/analyses/assembly/Scleronephthya_newbler_ssmith_07May11/P_2011_04_07_15_57_03_runAssembly/assembly/454Exemplar.fna --species "Scleronephthya" --ncbi_id "285276" --itis_id "52034" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 by SSmith. Cataloged the exemplars SSmith already selected.  ITIS id for family" --sample_prep " | "

wget ftp://ftp.ncbi.nih.gov/genomes/Strongylocentrotus_purpuratus/RNA/rna.fa.gz
gunzip rna.fa.gz
agalma catalog insert --id "NCBI_STRPUR" --paths rna.fa --species "Strongylocentrotus purpuratus" --ncbi_id "7668" --itis_id "157975" --library_type "genome" --note "Downloaded gene predictions from NCBI" --sample_prep " | "

wget ftp://ftp.ensembl.org/pub/release-71/fasta/taeniopygia_guttata/cdna/Taeniopygia_guttata.taeGut3.2.4.71.cdna.all.fa.gz
gunzip Taeniopygia_guttata.taeGut3.2.4.71.cdna.all.fa.gz
agalma catalog insert --id "NHGRI_TAEGUT" --paths Taeniopygia_guttata.taeGut3.2.4.71.cdna.all.fa --species "Taeniopygia guttata" --ncbi_id "59729" --itis_id "563276" --library_type "genome" --note "Downloaded gene predictions from NHGRI" --sample_prep " | "

wget ftp://ftp.jgi-psf.org/pub/JGI_data/Trichoplax_adhaerens_Grell-BS-1999/annotation/v1.0/Triad1_best_transcripts.fasta.gz
gunzip Triad1_best_transcripts.fasta.gz
agalma catalog insert --id "JGI_TRIADH" --paths Triad1_best_transcripts.fasta --species "Trichoplax adhaerens" --ncbi_id "10228" --itis_id "696105" --library_type "genome" --note "Best transcript predictions from JGI" --sample_prep " | "

