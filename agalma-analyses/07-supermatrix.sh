#!/bin/sh
#SBATCH -J supermatrix
#SBATCH -t 2:00:00
#SBATCH -c 16
#SBATCH --mem=60g

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-cnidaria2014.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-cnidaria2014.sqlite,outdir=/gpfs/data/cdunn/analyses/cnidaria2014,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"


ID=PhylogenyTree

cd ~/scratch
mkdir -p $ID
cd $ID

agalma treeprune --id $ID
agalma multalign --id $ID
agalma supermatrix --id $ID --proportion 0.5
