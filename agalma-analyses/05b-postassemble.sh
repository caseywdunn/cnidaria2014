#!/bin/sh
#SBATCH -J postassemble_external
#SBATCH -t 24:00:00
#SBATCH -c 16
#SBATCH --mem=60G
#SBATCH --array=1-29

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-cnidaria2014.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-cnidaria2014.sqlite,outdir=/gpfs/data/cdunn/analyses/cnidaria2014,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"


IDS=(
	454_ANTGRI
	454_ANTHOM
	454_AURAUR
	454_CALFRA
	454_CANDEL
	454_CERIAN
	454_CRASPE
	454_HALICL
	454_HALVAL
	454_ISIDEL
	454_NEPHTH
	454_OBELON
	454_SCLERO
	FLYBASE_DROMEL
	JGI_AMPQUE
	JGI_CAPTE1
	JGI_NEMVEC
	JGI_TRIADH
	NCBI_HYDMAG
	NCBI_STRPUR
	NHGRI_MNELEI
	NHGRI_TAEGUT
	OIST_ACRDIG
	dbEST_ANEVIR
	dbEST_CLYHEM
	dbEST_METSEN
	dbEST_MONFAV
	ext_ACRMIL
	ext_PORAST
)

ID=${IDS[$SLURM_ARRAY_TASK_ID-1]}
echo $ID
agalma catalog search $ID

cd ~/scratch
mkdir -p $ID
cd $ID

agalma postassemble --id $ID --external
agalma load --id $ID

