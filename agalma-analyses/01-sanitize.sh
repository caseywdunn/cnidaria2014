#!/bin/sh
#SBATCH -J cnidaria_sanitize
#SBATCH -t 24:00:00
#SBATCH --array=1-12

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-cnidaria2014.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-cnidaria2014.sqlite,outdir=/gpfs/data/cdunn/analyses/cnidaria2014,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"

IDS=(
	HWI-ST625-54-C026EACXX-6-ATCACG
	HWI-ST625-54-C026EACXX-3-TATC
	HWI-ST625-73-C0JUVACXX-7-AGALMA
	SRX231866
	HWI-ST625-61-C02G3ACxx-6-GGCTAC
	HWI-ST625-54-C026EACXX-7-CTTGTA
	HWI-ST420-69-D0F2NACXX-3-ECTOPLEURA
	HISEQ-129-C25BFACXX-8-HYDRACTINIA
	HWI-ST625-51-C02UNACXX-7-GATCAG
	HWI-ST625-73-C0JUVACXX-7-TTAGGC
	HISEQ-134-C27MTACXX-2-PODOCORYNA
	HISEQ-168-C3DEYACXX-8-ALATINA
)

ID=${IDS[$SLURM_ARRAY_TASK_ID-1]}
echo $ID
agalma catalog search $ID

cd ~/scratch
mkdir -p $ID
cd $ID

agalma sanitize --id $ID
