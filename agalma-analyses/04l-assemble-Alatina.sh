#!/bin/sh
#SBATCH -J alatina_assemble
#SBATCH -t 24:00:00
#SBATCH -c 16
#SBATCH --mem=60G

set -e


export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-cnidaria2014.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-cnidaria2014.sqlite,outdir=/gpfs/data/cdunn/analyses/cnidaria2014,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"

ID=HISEQ-168-C3DEYACXX-8-ALATINA

echo $ID
agalma catalog search $ID

cd ~/scratch
mkdir -p $ID
cd $ID

agalma assemble --id $ID 

